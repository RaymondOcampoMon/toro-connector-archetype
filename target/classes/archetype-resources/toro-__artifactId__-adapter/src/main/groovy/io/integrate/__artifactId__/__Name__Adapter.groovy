package io.integrate.${artifactId}

import io.toro.${artifactId}.models.*
import io.toro.${artifactId}.response.*
import io.toro.${artifactId}.connector.*

import org.springframework.web.bind.annotation.*

import com.toro.esb.core.api.APIResponse

/**
 *
 * @author is.me
 *
 */
@ResponseBody
@RequestMapping( value = 'loremipsum', produces = ['application/xml', 'application/json'] )
public class ${Name}Adapter {

	@ResponseBody
	@RequestMapping(
	value = 'getting-bacon',
	method = [RequestMethod.GET] )
	public APIResponse bacon() {
		List response = alias.${camelcasename}Get()
		new APIResponse('OK', response)
	}
}
