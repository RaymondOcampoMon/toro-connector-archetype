
import io.toro.${artifactId}.connector.${Name}Connector
import io.toro.${artifactId}.exception.${Name}Exception
import io.toro.${artifactId}.request.*
import io.toro.${artifactId}.model.*

import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Stepwise
import spock.lang.Unroll

import com.toro.licensing.LicenseManagerProperties
import com.toro.licensing.provider.ProductKeyProvider
import com.toro.licensing.provider.ToroPublicKeyPasswordProvider

@Stepwise
@Unroll
class ${Name}Test extends Specification { 
	
//	static {
//		LicenseManagerProperties.setProductKeyProvider( new ProductKeyProvider( 'esb' ) );
//		LicenseManagerProperties.setLicensePasswordProvider( new ToroPublicKeyPasswordProvider(
//				'****COMMODORE64BASICV2****64KRAMSYSTEM38911BASICBYTESFREEREADY.' ) )
//	}

	@Shared
	def alias = "yourAlias"

	/**
	 * ${Name}GET RELATED METHODS
	 */
	@Shared def response = null
	def '${camelcasename}Get-test'() {
		when:
		response = ${Name}Connector.${camelcasename}Get( [origin: '124.106.157.20'] )
		then:
		response != null
	}

}
