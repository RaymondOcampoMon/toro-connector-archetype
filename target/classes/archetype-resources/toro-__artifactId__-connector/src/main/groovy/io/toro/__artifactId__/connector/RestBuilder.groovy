package io.toro.${artifactId}.connector

import io.toro.${artifactId}.exception.${Name}Exception

import groovyx.net.http.ContentType
import groovyx.net.http.HttpResponseDecorator
import groovyx.net.http.HttpResponseException
import groovyx.net.http.RESTClient

import com.fasterxml.jackson.annotation.JsonInclude.Include
import com.fasterxml.jackson.databind.ObjectMapper

/**
 * @author is.me
 */
class RestBuilder {

	private static final ObjectMapper MAPPER = new ObjectMapper().setSerializationInclusion( Include.NON_NULL )
	String alias
	String method
	String path
	String defaultUri = 'https://httpbin.org'
	ContentType accept = ContentType.JSON
	ContentType contentType = ContentType.JSON
	Map queries = [:]
	Map request = [:]
	Map headers = [:]

	Class clazz

	RestBuilder addAlias( String alias ) {
		this.alias = alias
		this
	}

	RestBuilder addMethod( String method ) {
		this.method = method
		this
	}

	RestBuilder addPath( String path ) {
		this.path = path
		this
	}

	RestBuilder addHeader( Map map ) {
		this.headers += map
		this
	}

	//overload to add basic auth with encoded input or one input
	RestBuilder addAuthHeader( String input ) {
		this.headers += [ 'Authorization': input]
		this
	}

	//overload to add a specific auth from toro.properties
	RestBuilder addAuthHeader( String alias, String property ) {
		String key = ${Name}Connector.getToroProperties( alias )[ property ]
		this.headers += [ 'Authorization': key]
		this
	}

	//overload to add basic auth with user pass.
	@SuppressWarnings('UnnecessaryGetter')
	RestBuilder getBasicAuthHeader( String alias ) {
		Map prop = ${Name}Connector.getToroProperties(alias)
		String auth = prop[0] + ':' + prop[1]
		this.headers += ['Authorization': 'Basic ' + auth.getBytes().encodeBase64()]
		this
	}

	RestBuilder addRequest( Map request ) {
		this.request += request
		this
	}

	//overload to recieve generic object request
	RestBuilder addRequest( Object request ) {
		this.request += MAPPER.convertValue( request, Map )
		this
	}

	RestBuilder addQuery( Map map ) {
		this.queries += map
		this
	}
	//overload to recieve generic object query
	RestBuilder addQuery( Object object ) {
		this.queries += MAPPER.convertValue( object, Map )
		this
	}

	RestBuilder addResponse( Class clazz ) {
		this.clazz = clazz
		this
	}

	RestBuilder addUri( String uri ) {
		this.defaultUri = uri
		this
	}

	@SuppressWarnings('UnnecessaryPublicModifier')
	public <T> T build() {
		contact()
	}

	private <T> T contact() throws ${Name}Exception {
		headers << ['Accept': accept]
		new ${Name}Connector().${camelcasename}licenseManager()
		RESTClient client = new RESTClient( defaultUri )
		HttpResponseDecorator response
		try {
			Map params = [ path: path, headers: headers, contentType: contentType ]
			if ( queries ) {
				params.query = queries
			}
			if ( request ) {
				params.body = request
			}
			response = client."${method}" ( params ) as HttpResponseDecorator
			Map json = response.data as Map
			MAPPER.convertValue( json, clazz )
		} catch ( HttpResponseException ex ) {
			throw new ${Name}Exception( ex.message )
		}
	}
}
