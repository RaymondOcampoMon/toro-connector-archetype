package io.toro.${artifactId}.models

import groovy.transform.ToString
import com.fasterxml.jackson.annotation.JsonProperty

/**
 * @author is.me
 */
@ToString( includeNames = false)
class Headers {
	@JsonProperty('Accept')
	String accept
	@JsonProperty('Accept-Encoding')
	String acceptEncoding
	@JsonProperty('Content-Length')
	String contentLength
	@JsonProperty('Content-Type')
	String contentType
	@JsonProperty('Host')
	String host
	@JsonProperty('Origin')
	String origin
	@JsonProperty('Referer')
	String referer
	@JsonProperty('User-Agent')
	String userAgent

}
