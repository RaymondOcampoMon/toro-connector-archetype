package io.toro.${artifactId}.response

import io.toro.${artifactId}.models.Headers

/**
 * @author is.me
 */
class HttpBinGetResponse {
	Object args
	Headers headers
	String origin
	String url

}
