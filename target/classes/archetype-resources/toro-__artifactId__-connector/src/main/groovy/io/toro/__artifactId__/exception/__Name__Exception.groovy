package io.toro.${artifactId}.exception

/**
 * @author me
 */
class ${Name}Exception extends Exception {

	${Name}Exception (String message ) {
		super( message )
	}

	${Name}Exception (Throwable t ) {
		super( t )
	}
}
