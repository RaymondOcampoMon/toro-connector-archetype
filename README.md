#Connector Archetype

A Connector Archetype provides users with the means to generate a templated adapter with connector.

## Requirements
  - xml catalog of the archetype
  - IDE w/ maven
  

## HOW TO USE

- ####Add catalog.xml in local / repo. The catalog should atleast look like this.

```
<?xml version="1.0" encoding="UTF-8"?>
<archetype-catalog>
  <archetypes>
    <archetype>
    <groupId>io.toro</groupId>
    <artifactId>connector-archetype</artifactId>
    <version>0.0.1-SNAPSHOT</version>
    <description>Archetype for creating Adapter with Connector</description>
    </archetype>
  </archetypes>
</archetype-catalog>
```

- ####In eclipse, create maven project, uncheck "create simple project(skip archetype)"
![step1.jpg](https://bitbucket.org/repo/nMRz9o/images/966616821-step1.jpg)
![step2.5.jpg](https://bitbucket.org/repo/nMRz9o/images/1402952398-step2.5.jpg)
![step2.jpg](https://bitbucket.org/repo/nMRz9o/images/2729014285-step2.jpg)


- ####Choose your catalog (may check "include snapshot archetypes" if your catalog is in a snapshot version)
![step3.jpg](https://bitbucket.org/repo/nMRz9o/images/910744337-step3.jpg)
![step4.jpg](https://bitbucket.org/repo/nMRz9o/images/2967483785-step4.jpg)

- ####Fill all the variables needed

##Options
| Option        | Description   |
| ------------- |-------------|
| groupId     | your group id (example: io.toro)  |
| artifactId     | this should only be the lower case of your connector name (example: httpbin)  |
| version | the version of the poms of the project  |
| Name     | this should be the name of your connector (example: HttpBin) |
| camelcasename     | this should be the camel cased version of your connector name (example: httpBin)  |
| domain | your default domain uri, may input variables inside (example: https://httpbin.org or https://${var}.com)  |
| extensionmodule | this should be left as its default value |

![step5.png](https://bitbucket.org/repo/nMRz9o/images/1156166249-step5.png)

- ####And your connector is ready to go.