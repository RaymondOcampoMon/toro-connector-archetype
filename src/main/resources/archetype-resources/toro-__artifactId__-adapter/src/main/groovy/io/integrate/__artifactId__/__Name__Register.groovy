package io.integrate.${artifactId}

import io.toro.${artifactId}.exception.*

import org.apache.commons.lang3.StringUtils
import org.springframework.web.bind.annotation.*

@RestController
public class ${Name}Register {

	static String SaveAccessDetails( String alias, String id, String secret ) {
		if ( StringUtils.isBlank( alias ) )
			throw new ${Name}Exception( 'ERROR: Please enter an alias' )
		if ( StringUtils.isBlank( id ) )
			throw new ${Name}Exception( 'ERROR: Please enter an id' )
		if ( StringUtils.isBlank( secret ) )
			throw new ${Name}Exception( 'ERROR: Please enter an secret' )
		"${artifactId}.${alias}".saveTOROProperty( "${id.trim()}||${String.trim()}" )
		"Successfully saved alias ${alias}."
	}
}