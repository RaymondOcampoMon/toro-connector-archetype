package io.toro.${artifactId}.connector

import io.toro.${artifactId}.exception.*
import io.toro.${artifactId}.models.*
import io.toro.${artifactId}.response.*

import org.apache.commons.lang3.StringUtils
import groovy.transform.CompileStatic
import groovy.transform.TypeChecked
import groovy.transform.TypeCheckingMode

import com.toro.licensing.LicenseManager

/**
 * See <a href="https://resttesttest.com/">here</a>
 * @author is.me
 */
@CompileStatic
@SuppressWarnings(['LineLength', 'DuplicateMapLiteral'])
@TypeChecked(TypeCheckingMode.SKIP)
class ${Name}Connector {

	@SuppressWarnings( 'UnnecessaryGetter' )
	private static String[] getCredentials( String alias) {
		String credentials = "stormpath.${alias}".getTOROProperty()
		credentials.split('\\|\\|')
	}

	@SuppressWarnings([ 'UnnecessaryGetter', 'ConnectorParameterRule', 'ConnectorJavadocRule' ])
	static ${camelcasename}licenseManager() {
		LicenseManager.getInstance().getLicense()
	}
	//this is used for saving credentials to toroproperties which have an id and a secret, or a username and a password
	/**
	 * Save access details to toroproperty
	 * <br/>See <a href="https://resttesttest.com/#">Here</a>
	 * @param alias preconfigured alias of the user pre-configured alias
	 * @param id id given to you by the API
	 * @param secret secret given to you by the API
	 * @return "Successfully saved alias" if success.
	 */
	static String ${camelcasename}SaveCredentials( String alias, String id, String secret ) {
		if ( StringUtils.isBlank( alias ) )
			throw new ${Name}Exception( 'ERROR: Please enter an alias' )
		if ( StringUtils.isBlank( id ) )
			throw new ${Name}Exception( 'ERROR: Please enter an id' )
		if ( StringUtils.isBlank( secret ) )
			throw new ${Name}Exception( 'ERROR: Please enter an secret' )
		"stormpath.${alias.trim()}".saveTOROProperty( "${id.trim()}||${secret.trim()}" )
		"Successfully saved alias ${alias}."
	}

	/**
	 * httpBinGet tests for get property
	 * See <a href="https://resttesttest.com/">here</a>
	 * @param query queries
	 * @return fetched data
	 */
	@SuppressWarnings('ConnectorParameterRule')
	static HttpBinGetResponse ${camelcasename}Get( Map<String, String> query ) {
		new RestBuilder()
			.addMethod( 'get' )
			.addPath( '/get' )
			.addQuery( query )
			.addResponse( HttpBinGetResponse )
			.build()
	}

//	public static void main(args) {
//		String username = 'user'
//		String password = 'pass'
//		loremIpsumSaveCredentials( 'loremIpsumhAlias', username, password )
//		String alias = 'test'
//		println httpBinGet( [origin: '124.106.157.20'] ).url
//	}

}
