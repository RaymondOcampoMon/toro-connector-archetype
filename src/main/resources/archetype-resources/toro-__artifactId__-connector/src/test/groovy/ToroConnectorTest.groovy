import io.toro.codenarc.ant.TOROCodenarcAntTask

class ToroConnectorTest extends TOROCodenarcAntTask {

	@Override
	String getRulesetFiles() {
		'rulesets/toro_connector_codenarc.xml'
	}

	@Override
	int getMaxPriority3Violations() {
		5
	}
}